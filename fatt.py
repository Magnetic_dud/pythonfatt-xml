#!/usr/bin/env python
import xml.etree.ElementTree as ET

#funzione che scrive una riga ogni volta che viene chiamata
def scriviRiga():
	row = ET.SubElement(rows, "Row")
	riga_ddt = input("Riga descrizione DDT? (s/N) ")
	if riga_ddt.startswith('s'):
		numero_ddt = input("Numero DDT? ")
		data_ddt = input("data DDT in formato GG/MM/AAAA ")
		ET.SubElement(row, "Description").text = "DDT n. {} del {}".format(numero_ddt,data_ddt)
	else:
		codice_prodotto = input("Codice prodotto? ")
		if (codice_prodotto):
			ET.SubElement(row, "Code").text = codice_prodotto
		descrizione_prodotto = input("Descrizione prodotto? ")
		if (descrizione_prodotto):
			ET.SubElement(row, "Description").text = descrizione_prodotto
		quantita = input("Quantità? ")
		if (quantita):
			ET.SubElement(row, "Qty").text = quantita
		unita_di_misura = input("Unità di misura? ")
		if (unita_di_misura):
			ET.SubElement(row, "Um").text = unita_di_misura
		prezzo = input("Prezzo? ")
		sconto = input("Sconto? ")
		if (sconto):
			ET.SubElement(row, "Discounts").text = sconto
		if (prezzo):
			ET.SubElement(row, "Price").text = prezzo.replace(',','.')
			ET.SubElement(row, "VatCode", Perc="22", Class="Imponibile", Description="Imponibile 22%").text = "22"

#il programma inizia qui
root = ET.Element("EasyfattDocuments")
documents = ET.SubElement(root, "Documents")
document = ET.SubElement(documents, "Document")

#intestazione fattura
codice_cliente = input("Inserisci il codice cliente su EasyFatt: ")
ET.SubElement(document, "CustomerCode").text = codice_cliente.zfill(4)
ET.SubElement(document, "DocumentType").text = "I"
data_fattura = input("Inserisci la data fattura in formato AAAA-MM-GG: ")
ET.SubElement(document, "Date").text = data_fattura
numero_fattura = input("Inserisci il numero fattura: ")
ET.SubElement(document, "Number").text = numero_fattura
sezionale = input("Inserisci il sezionale fattura: ")
if (sezionale):
	if not sezionale.startswith('/'):
		sezionale='/'+sezionale
	ET.SubElement(document, "Numbering").text = sezionale.upper()
prezzi_iva_inclusa = input("I prezzi che inserirai sono IVA inclusa? (s/N) ") or "false"
if prezzi_iva_inclusa.startswith('s'):
	prezzi_iva_inclusa="true"
else:
	prezzi_iva_inclusa="false"
ET.SubElement(document, "PricesIncludeVat").text = prezzi_iva_inclusa
commenti_fattura = input("Hai un commento su questa fattura? ")
if (commenti_fattura):
	ET.SubElement(document, "InternalComment").text = commenti_fattura

rows = ET.SubElement(document, "Rows")

while True:
	#inserisci una riga documento
    scriviRiga()
    ripeti = input("Aggiungi una nuova riga? s/N ")
    if ripeti.startswith('s'):
		#si fa un altro giro
        continue
    else:
        print("File generato!")
        #si esce dal loop infinito
        break

tree = ET.ElementTree(root)
ET.indent(tree, space=" ", level=0)
tree.write("fattura{}_del_{}.DefXml".format(numero_fattura,data_fattura),encoding="UTF-8",xml_declaration=True,short_empty_elements=False)
