# Pythonfatt-xml

Obiettivo del progetto: convertire le fatture da Fattureincloud.it a Easyfatt.

Progetto Open Source, tutti i contributi sono benvenuti

## Requisiti

* Python da 3.9 in su

Da convertire quindi l'esportazione in excel a easyfatt-xml. Come funziona al momento: esporti un mese di fatture da fattureincloud.it in Excel, poi partendo dal fondo si copia riga per riga sulla riga di comando. É palloso ma comunque più veloce che farlo a mano con la GUI di Easyfatt.

Al momento non sono in grado di convertire direttamente da excel, ma riga per riga tramite utente. Chissà se il programma raggiungerà mai questo stadio utopico. Comunque essendo open source, è possibile contribuire allo sviluppo
